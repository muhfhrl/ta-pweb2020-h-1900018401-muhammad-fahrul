<?php
function initDB($db, $user, $pass, $port=3306) {
  $host = 'localhost';
  $pdo = new \PDO("mysql:host=$host;port=$port;dbname=$db;",$user,$pass,[
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ
  ]);
  return $pdo;
}
$db = initDB('test','root','');
$db->query('CREATE TABLE IF NOT EXISTS test (id INT PRIMARY KEY AUTO_INCREMENT, nama VARCHAR(100)) engine=innodb;');
$stmt = $db->query('SELECT COUNT(*) FROM test');
$jumbar = $stmt->fetch(\PDO::FETCH_NUM)[0];
$db->query("INSERT INTO test VALUES (NULL,'nama".++$jumbar."')");
$db->query("INSERT INTO test VALUES (NULL,'nama".++$jumbar."')");
$db->query("INSERT INTO test VALUES (NULL,'nama".++$jumbar."')");
$stmt = $db->query('SELECT * FROM test');
$hasil = $stmt->fetchAll();
echo "<pre>".print_r($hasil,true)."</pre>";
?>